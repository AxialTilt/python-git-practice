# python-git-practice

A repository to practice cloning, forking and merging with git.

## Instructions

1. Fork this repository
2. Clone your fork of this repository
3. Create a "hello, world!" program. Call it <YOUR_NAME>-helloworld.py
4. Commit your changes and push. [You can learn how to do this here!](https://www.tutorialspoint.com/git/git_basic_concepts.htm)
5. Go to GitLab and look at your repositories. Make sure you can see your changes.
6. Create a merge request to merge your repository into this one.
7. Your merge request will need to be approved.
8. Once your merge request has been approved, it'll be visible here!